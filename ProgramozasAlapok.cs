/*
	Ebben a fájlban megpróbálom összefoglalni, amiket a programozásról,
	illetve a C# nyelvről tanulunk, és ami fontos lehet.
*/


/*
using
-----
A using utasítással névtereket importálhatunk, hogy azok komponenseit ne kelljen
mindig a teljes nevükön megnevezni.

pl a kiíró függvény teljes nevén: System.Console.WriteLine() lenne, de ezután elég
*/
using System;

namespace Alapok
{
	class ProgramozasAlapok
	{
		public static void Main()
		{
		/*
			Programfájlok
		    -------------
		    A programfájlok egyszerű szövegfájlok, melyeknek .cs a kiterjesztése

			A C# nyelv
		    ----------
		    A C# egy fordított nyelv, ami azt jelenti, hogy a programfájlból
		    először futtatható, .exe fájlt kell készítenünk a fordítás során,
		    és csak azt tudja a számítógép végrehajtani.

			A VS Express fejlesztőkörnyezetben ezt az F5 gombbal tudjuk elvégezni.

		    A programozási nyelvek részei
		    -----------------------------
			- utasítások

			Az utasítások a nyelv alapvető szavai, melyek a programok vezérléséhez,
			különböző dolgok definiálásához, leírásához használhatóak.
			Ilyenek például:
			class, for, if, while

			- változók, konstansok

			A változó adatok átmeneti tárolására szolgál. Egy változónak értéket adhatunk,
			műveleteket végezhetünk vele, és a C#-ban a változóknak típusuk van.
			A változókat használat előtt deklarálni kell. A deklaráció a következőképp néz ki:
			*/

			bool valami; //Tehát leírjuk a változó típusát (bool) és nevét (b).


			/*
			- konstansok
			Egy konstans egy olyan változó, amelynek csak a definíciókor adunk értéket,
			utána már nem változik meg az értéke.
			Pl:
			*/
			const double G = 9.80665;  // a nehézségi gyorsulás értéke a Földön a 45° földrajzi szélességen, tengerszinten
			/*

			- típusok

			Egy típus azt fejezi ki, hogy egy változóban milyen adatot tárolunk,
			valamint azt, hogy az ilyen típusú változókkal milyen műveleteket végezhetünk.

			egyszerű típusok:
			int		egész szám			1, 2, 1234, -123, 0
			bool	logikai érték		true, false
			string	unicode szöveg		"Csőváz", "", "şcoalǎ"
			double	lebegőpontos szám	3.1415, 0.1234, -3.01, 1.7E+3

			Például:
			*/

			int x, y; // ez két int típusú változó, egész számokat tartalmazhat.

			// A változók értéke kezdetben null (nem mindig, de általában)

			x = 1; // x értéke innentől 1 lesz. Az = jel az értékadást jelenti.
			y = 3;

			/*
			- operátorok

			Az operátorok műveleteket jelölnek. Lehetnek egy vagy két argumentumúak.
			Egy argumentuma van:
			!b				nem
			a++, ++a		eggyel növelés
			a--, --a		eggyel csökkentés

			Két argumentumúak:
			a || b		vagy
			a && b		és
			a + b		összeadás
			s + "F"		string összefűzés

			Összehasonlító operátorok:
			<, >, <=, >=, !=, ==

			Három argumentumú:
			(a == b) ? true : false			azaz: Ha (a==b), akkor true, egyébként false
			
			pl.
			*/

			valami = !true; // innentől kezdve b == false;

			x = x + 1;
			// x értékét megnöveltük eggyel.Ezt úgy is írhatjuk röviden, hogy:
			x++;

			y = x + y;
			//y-t x értékével növeljük. Ezt úgy is írhatjuk röviden, hogy:
			y += x;


			//Kiírunk egy sort a konzolra:
			Console.WriteLine ("Hello World");

			/**
			 * Vezérlőszerkezetek
			 * 
			 * blokk:
			 * { ... }
			 * 
			 * Csoportosításra szolgál.
			 * 
			 * elágazás
			 * --------
			 * A feltételtől függően hajtjuk végre az utasításokat:
			 */
			if (valami == false)
			{
				Console.WriteLine ("valami == hamis");
			}
			else if (y > 0)
			{
				Console.WriteLine ("valami == igaz, és y > 0");
			}
			else
			{
				Console.WriteLine ("valami == igaz, és y <= 0");
			}
			/**
			 * while ciklus
			 * ------------
			 * Amíg a feltétel igaz, addig ismétlődik az utasítás
			*/
			while (y < 1000)
			{
				++y;
			}

			/**
			 * for ciklus
			 * ----------
			 * speciális ciklus, pl az alábbi a következőt végzi:
			 * Először beállít egy i ciklusváltozót 0-ra, majd
			 * amíg a ciklusváltozó el nem éri a 10-et, végrehajtja a
			 * blokkban található utasításokat, aztán megnöveli i értékét 1-gyel.
			 */

			for (int i = 0; i < 10; ++i) {
				Console.WriteLine ("i értéke most: "+i);
			}

		}
	}

}
