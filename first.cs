using System;
using System.Text;

namespace test1
{
    // ez egy osztály. A fő osztály neve meg kell, hogy egyezzen a
    // fájl nevével.
    class First
    {
        static int kilenc = 9;

	// ennek az osztálynak kell, hogy legyen egy static void Main függvénye,
	// ez fog lefutni a program indításakor. Lehet egy string[] típusú argumentuma,
	// ami a parancssorból átadott paramétereket tartalmazza.
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");
            Console.WriteLine(kilenc);

            /* A stringek "összeadása" a konkatenáció */

            Console.WriteLine("A " + kilenc + " négyzete: "+Negyzet(9));

            /* Bela egy Ember típusú Objektum , mert példányosítottuk az Ember osztályt.*/
            Ember Bela = new Ember();

            Bela.knev = "Béla";
            Bela.vnev = "Kovács";
            Console.WriteLine(Bela.nev());
            Bela.setJelszo("","macska");

            Console.WriteLine("Béla jelszava macska? "+Bela.joJelszo("macska"));
            Console.WriteLine("Béla jelszava kutya? " + Bela.joJelszo("kutya"));
            Console.ReadKey();
        }

        static int Negyzet(int szam)
        {
            return (szam * szam);
        }
    }

    /* Ez egy osztály */
    class Ember{
        private static int labakSzama = 2;

	// a private kulcsszó azt jelenti, hogy csak az osztályon belül
	// fogjuk elérni a változót.        
        private int kor;

	// ezek a változók publikusak, így akárhonnan elérhetjük őket
        public string vnev;
        public string knev;

	// Ez a függvény visszaadja az ember nevét.
	// visszatérési értéke string, nincs paramétere.
        public string nev() {
            return vnev + " " + knev;
        }

        private string jelszo;
        public bool joJelszo(string ell_jelszo)
        {
            if (ell_jelszo == jelszo)
            {
                return true;
            }
            else {
                return false;
            }
        }

        // bool: true or false
        public bool setJelszo(string regiJelszo, string ujJelszo)
        {
            /* >, <, <=, >=, ==, != , true, false*/
            if (regiJelszo == jelszo || jelszo == null) 
            {
                jelszo = ujJelszo;
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}

