﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Lecke2
    {
        static void Main(string[] args)
        {
            string gyumolcs1;
            string gyumolcs2;
            char betu;
            int szam1;
            int szam2;
            gyumolcs1 = "alma";
            betu = 'a';
            szam1 = 19*17*2*5;
            szam2 = 19*2*3*3*3;
            Console.WriteLine(szam1 + "+" + szam2 + "=" + (szam1 + szam2));
            Console.WriteLine(szam1 + "x" + szam2 + "=" + (szam1*szam2));
            
            int oszto;
            oszto = lnko(szam1, szam2);
            Console.WriteLine("lnko" + "(" + szam1 + "," + szam2 + ")="+ oszto);
            Console.WriteLine(gyumolcs1);
            Console.WriteLine(7 / 4); //ez kiírja az egész osztás eredményét
            Console.WriteLine(7 % 4); //ez pedig a maradékot

			/* Ez fogja használni az 1. feladat függvényeit */
			double a = 1.0, b = 2.0, c = -15.0;
			Console.WriteLine ("A másodfokú egyenlet együtthatói: a = " + a + ", b = " + b + ", c = " + c + ".");
			Console.WriteLine ("Ekkor x1 = "+masodfoku1(a,b,c)+", x2 = "+masodfoku2(a,b,c));

			/* Ez pedig a 2. feladat függvényét */
			osztok (21893);

			Console.ReadKey();
        }

        private static int lnko(int szam1, int szam2)
        {
            int a, b;
            a = szam1;
            b = szam2;
            while (a != b)
            {
                Console.WriteLine("a=" + a + "," + "b=" + b);
                if (a > b)
                {
                    a = a - b;
                }
                else
                {
                    b = b - a;
                }
            }
            return a;

        }

        /* float, double ~ ezek valós számok ábrázolására való típusok */
        private static double gyok(double a)
        {
			//Ezzel ki tudod számolni "a" szám négyzetgyökét
            return Math.Sqrt(a);
        }

        private static int maradek(int a, int b)
        {
			//Ezzel pedig az a/b osztás maradékát kapod vissza
            return a % b;
        }


        /*
         1. Feladat:
         Adja vissza az alábbi két függvény az ax^2 + bx + c = 0 egyenlet gyökeit
         */
         private static double masodfoku1 (double a, double b, double c){
			return 0.0; 
         }
		 private static double masodfoku2 (double a, double b, double c){
			return 0.0; 
		 }
         
		/*
         2. Feladat
		 írja ki az alábbi függvény az "a" szám összes osztóját!
		 */
         private static void osztok (int a){
          
         }
         
     }
}
